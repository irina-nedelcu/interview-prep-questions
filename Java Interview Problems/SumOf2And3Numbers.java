// write a program that tells you if an element can be found in a certain array
public class SumOf2And3Numbers {
	
	// element and arrays used to test the code
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = {1,2,3,4};
		int[] array2 = {};
		int[] array3 = {1,4,6,7};
		int element = 3;
		System.out.println(Alexandru(element, array));
		System.out.println(Alexandru(element, array2));
		System.out.println(Alexandru(element, array3));
	}

	// create a for loop that goes through each element; 
	// it returns true if it finds the element, otherwise it returns false
	public static boolean Alexandru(int element, int[] array){
		for(int iter = 0; iter < array.length; iter++){
			if(element == array[iter]){
				return true;
			}
		}
		return false;
	}
}
