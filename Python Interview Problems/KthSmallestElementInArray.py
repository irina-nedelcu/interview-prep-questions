'''
Created on May 28, 2017

@author: Irina
'''

#############################################################################
# Find the Kth smallest element in an array

#############################################
# version 1, without sorting the entire array
array = [1,4,2,5,6,3,9,1,1,10,26,27]
def SmallestElement(k, array):
    if array == []:
        return False
    if k > len(array):
        return False
    
    # form an array that contains k elements and sort it
    array2 = array[0:k]
    array2.sort()
    index = k
    
    # check if each element after position k in the initial array is smaller than elements in array2
    while index < len(array):
        if array[index] < array[index-1]:
            for i in range(0, len(array2)):
                if array[index] <= array2[i]:
                    array2.insert(i, array[index])
                    array[index] = array[index-1]
        index = index + 1    
    return array2[k-1]

print("The smallest element in this array is ", SmallestElement(5, array))


#####################################
# version 2, sorting the entire array
array = [1,4,2,5,6,3,9,1,1,10,26,27]

def SmallestElement2(k, array):
    if array == []:
        return False
    if k > len(array):
        return False
    array.sort()
    return array[k-1]

print("The smallest element in this array is ", SmallestElement2(5, array))

# TEMA:
# 1) make this problem with swapping
# 2) Make it without sorting -> linked list
