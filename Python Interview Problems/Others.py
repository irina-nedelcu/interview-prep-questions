'''
Created on May 27, 2017

@author: Irina
'''

# range(0,1)
for i in range(0,1):
    print(i)   ## prints 0

# range(0,0)
for i in range(0,0):
    print(i)   ## null

# is vs ==
for i in range(250, 260):
    a = i
    print(i, a is int(str(i))) # is works for equal numbers within this range: -5 to 256
