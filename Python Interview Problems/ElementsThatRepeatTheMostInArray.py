'''
Created on May 28, 2017

@author: Irina
'''

#############################################################################
# Find the elements that repeat the most in an array

array = [1,4,7,3,2,2,7,2,4,6,1,2,2]
def Alexandru(array):
    if array == []:
        return False
    
    # sort the array
    array.sort()
    
    # calculate how many times each element repeats
    # calculate the difference between each neighboring values, if the difference is 0, then we add 1 to nb_repetitions; 
    # nb_repetitions resets everytime its value is not 0
    output = []
    max_repetitions = 1
    nb_repetitions = 1
    for index in range(1,len(array)):
        is_repeated = array[index] - array[index-1]
        if is_repeated == 0:
            nb_repetitions = nb_repetitions + 1
            if max_repetitions == nb_repetitions:
                output.append(array[index])
        else:
            if nb_repetitions > max_repetitions:
                max_repetitions = nb_repetitions
                output = [array[index]]
            nb_repetitions = 1
    return max_repetitions

print(array)
print(Alexandru(array))
    