'''
Created on May 28, 2017

@author: Irina
'''
#############################################################################
# Find the missing number in an array


array = [16,8,4,9,15,14,13,3,5,6,7,10,11] # missing number is 12

# sort the array
array.sort()
print(array)


# calculate the difference between each neighboring numbers in the array
# when it finds the difference == 2, the while loop will stop
def MissingNumber(array):
    index = 1
    length_array = len(array)
    difference = 1
    while difference == 1:
        difference = array[index] - array[index-1]
        index = index + 1
        if length_array == index - 1:
            difference = 0
    return (array[index-1]-1)
    

def MissingNumber2(array):
    index = 0
    while array[index+1] - array[index] <= 1:
        index = index + 1
    return array[index+1]-1
    
print(MissingNumber(array))
print(MissingNumber2(array))


# what if the array has all numbers?
# Make this a function

# 1) what if there are more missing numbers?
# 2) make the same problem without sorting.
     # -> big O of 2n (without any extra arrays).
     # -> big O of n (you can use one or two extra arrays).

