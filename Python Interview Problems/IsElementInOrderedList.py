'''
Created on May 27, 2017

@author: Irina
'''

#############################################################################
# you have an array that consists of ordered numbers
# find an efficient way to find out whether an element is found in this array

from math import ceil, floor, log2
def Alexandru2(element, array):
    if len(array) == 0:
        return False
    i = floor(len(array)/2)
    length_array = len(array)
    max_splits = floor(log2(length_array))
    splits = 0
    i = int(i)
    while element != array[i]:
        splits = splits+1
        if splits == max_splits + 1:
            return False
        if element < array[i]:
            array = array[0:i]
        else:
            array = array[i:len(array)]
        i = ceil(len(array)/2)
    return True

array = [3,7,9,10,15,19,20,25,26,27,29,30,35,39]
array2 = []
element = 12
element2 = 9
element3 = 28
element4 = 35
element5 = 20

print(Alexandru2(element, array2))
print(Alexandru2(element, array))
print(Alexandru2(element2, array))
print(Alexandru2(element3, array))
print(Alexandru2(element4, array))
print(Alexandru2(element5, array))