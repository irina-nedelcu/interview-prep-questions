'''
Created on May 27, 2017

@author: Irina
'''

#############################################################################
#Find all pairs of 2 and 3 numbers in an array whose sum is n.
#Find all pairs of 2 and 3 numbers in an array whose sum is n.

array = [1,2,4,3,4,7,5,3,2,13,15]

# version 1 
# sorting the array and then finding the sum of the pairs

print(len(array))
i = 0
k = 0
n = 10
array.sort()
print(array)
print(n)
if array == []:
    output = False # make consistent data type for return
output = []
while array[i] < (n+1):
    summation = array[i]
    i = i+1
    j = i  # add if statement to check for array of 1 element
    while array[j] + array[i-1] < (n+1):
        summation = array[i-1] + array[j]
        if summation == n:
            m = [array[i-1], array[j]]
            output.append(m)
        j = j+1
        p = j
        if p < len(array):
            while (array[p] + array[j-1] + array[i-1]) < (n+1):
                summation = array[p] + array[j-1] + array[i-1]
                if summation == n:
                    m = [array[i-1], array[j-1], array[p]]
                    output.append(m)
                p = p + 1
        if j == len(array) - 1:
            break
    if i == len(array)-1:
        break

print(output)



# version 2
# finding the sum of the pairs without sorting
array = [1,2,4,3,4,7,5,3,2,13,15]
n = 10
print()
print(array)
print(n)
if array == []:
    output = False # make consistent data type for return
output = []
for index in range(0,len(array)-1):
    for index2 in range(index+1,len(array)):
        if (array[index] + array[index2]) == n:
            m = [array[index], array[index2]]
            output.append(m)
        for index3 in range(index2+1,len(array)):
            if (array[index] + array[index2] + array[index3]) == n:
                m = [array[index], array[index2], array[index3]]
                output.append(m)
print(output)
  
 


