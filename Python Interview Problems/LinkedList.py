'''
Created on May 30, 2017

@author: Irina
'''

# Declare a Linked List
class ListNode:
    def __init__(self,value,pointer):
        self.value = value
        self.pointer = pointer
        

# Declare the instances of the nodes
node4 = ListNode(10, None)
node3 = ListNode(9, node4)
node2 = ListNode(3, node3)
node1 = ListNode(19, node2)


# Print the node values
print(node1.value)
print(node3.value)
print(node4.value)
print(node2.value)
print()

def PrintNodes():
    print("[", node1.value, "] -> [", node2.value, "] -> [", node3.value, "] -> [", node4.value, "] -> NULL" )
PrintNodes()
print()


# global variable that will be the starting point below
HeadNode = node1


# print all the values in a linked list using a loop when you don't know the exact number of nodes
def PrintNodesLoop():
    global HeadNode # this variable was declared outside the function, so we'll call it by putting global in front of it
    Current = HeadNode
    CountNodes = 0
    if(Current != None):
        while(Current.pointer != None):
            print(Current.value)
            Current = Current.pointer
            CountNodes = CountNodes + 1
        CountNodes = CountNodes + 1
        print(Current.value)
        print("Count: ", CountNodes)
    else:
        print("Empty List")       
PrintNodesLoop()
print()

# create an empty linked list
def CreateEmptyList():
    global HeadNode
    HeadNode = None


# delete a linked list
# same as creating an empty list
def DeleteList():
    global HeadNode
    HeadNode = None
    

# Check if a linked list is empty
def IsListEmpty():
    global HeadNode
    return HeadNode == None
print("List is empty: ", IsListEmpty())
print()


# find a node in a list
def FindANode(ValueWanted):
    global HeadNode
    Current = HeadNode
    while((Current.pointer != None) and (Current.value != ValueWanted)):
        Current = Current.pointer
    if(Current.pointer == None):
        print(ValueWanted, "has not been found in the list.")
    else:
        print("The value", ValueWanted, "was found.")
FindANode(9)
print()


# insert a node into the list
def InsertANode(PositionWanted, ValueWanted):
    global HeadNode
    Current = HeadNode
    nodeX = ListNode(ValueWanted, None)
    PositionCounter = 1
    if(PositionWanted == 0):
        HeadNode = nodeX
        nodeX.pointer = Current
    else:
        while(PositionWanted > PositionCounter):
            Current = Current.pointer
            PositionCounter = PositionCounter + 1
        nodeX.pointer = Current.pointer
        Current.pointer = nodeX
PrintNodesLoop()
print()
InsertANode(3, 100)
PrintNodesLoop()
print()


# delete a node
def DeleteANode(ValueToDelete):
    global HeadNode
    Previous = None
    Current = HeadNode
    if Current.value == ValueToDelete:
        HeadNode = Current.pointer
    else:
        while((Current.pointer != None) and (Current.value != ValueToDelete)):
            Previous = Current
            Current = Current.pointer
        Previous.pointer = Current.pointer
DeleteANode(10)
PrintNodesLoop()


# Resources:
# https://www.youtube.com/watch?v=j0-YASYqOFw&t=845s
    