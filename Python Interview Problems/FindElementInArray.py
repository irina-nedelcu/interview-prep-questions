'''
Created on May 27, 2017

@author: Irina
'''

# write a program that tells you if an element can be found in a certain array


element = 3
array = [1,2,3,4]
array2 = []
array3 = [1,4,6,7]

# version 1
def Alexandru(element, array):
    for i in range(0, len(array)):
        if element == array[i]:
            return True
    return False

print(Alexandru(element, array))
print(Alexandru(element, array2))
print(Alexandru(element, array3))

# version 2
def Alexandru2(element, array):
    for number in array:
        if element == number:
            return True
    return False

print(Alexandru2(element, array))
print(Alexandru2(element, array2))
print(Alexandru2(element, array3))
